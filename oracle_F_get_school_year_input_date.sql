--------------------------------------------------------
-- UDF to calculate the current school year. Originally designed by max.turavani@dsbn.org
-- Update by Patrick.Aubin@cspne.ca to Oracle syntax
--------------------------------------------------------
CREATE OR replace FUNCTION F_get_school_year_input_date ( 
input_date DATE DEFAULT SYSDATE) 
RETURN VARCHAR2 
AS 
BEGIN 
    DECLARE 
        school_year VARCHAR2(8); 
    BEGIN 
        IF ( Extract(month FROM input_date) ) BETWEEN 1 AND 7 THEN 
          school_year := ( ( Extract(year FROM input_date) ) - 1 ) 
                         ||( Extract(year FROM input_date) ); 
        ELSIF ( Extract(month FROM input_date) ) BETWEEN 8 AND 12 
              AND ( Extract(year FROM input_date) ) = ( Extract(year FROM 
                                                                SYSDATE) ) 
        THEN 
          school_year := ( ( Extract(year FROM input_date) ) 
                           || ( ( Extract(year FROM input_date) ) + 1 ) ); 
        ELSIF ( Extract(month FROM input_date) ) BETWEEN 8 AND 12 
              AND ( Extract(year FROM input_date) ) < ( Extract(year FROM 
                                                                SYSDATE) ) 
        THEN 
          school_year := ( ( Extract(year FROM input_date) ) 
                           || ( ( Extract(year FROM input_date) ) + 1 ) ); 
        ELSE 
          school_year := ( ( Extract(year FROM input_date) ) 
                           || ( ( Extract(year FROM input_date) ) + 1 ) ); 
        END IF; 

        RETURN school_year; 
    END; 
END; 
