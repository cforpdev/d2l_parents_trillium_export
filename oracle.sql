--------------------------------------------------------
-- Export of student contacts for the BrightSpace parent portal
-- Originally designed by max.turavani@dsbn.org
-- Updated by Patrick.Aubin@cspne.ca to Oracle syntax
-- Rewritten by Pierre.Sarazin@cforp.ca to create temp tables to monitor data being sent
-- as well as log invalid contacts and possible duplicate contacts (2 entries in the person table for the same person)
--------------------------------------------------------


---------------------------------------------------------------------------------- 
-- We will create a temp table to store all parental records. We start by dropping the table if it exists
---------------------------------------------------------------------------------- 
BEGIN 
  EXECUTE IMMEDIATE 'DROP TABLE d2l_student_contacts'; 
EXCEPTION 
WHEN OTHERS THEN 
  IF SQLCODE != -942 THEN 
    RAISE; 
  END IF; 
END; 
---------------------------------------------------------------------------------- 
-- We will create a temp table to store invalid parental records (no contact info). We start by dropping the table if it exists
---------------------------------------------------------------------------------- 
BEGIN 
  EXECUTE IMMEDIATE 'DROP TABLE d2l_invalid_student_contacts'; 
EXCEPTION 
WHEN OTHERS THEN 
  IF SQLCODE != -942 THEN 
    RAISE; 
  END IF; 
END; 
---------------------------------------------------------------------------------- 
-- We will create a temp table to store invalid parental records (no contact info). We start by dropping the table if it exists
---------------------------------------------------------------------------------- 
BEGIN 
  EXECUTE IMMEDIATE 'DROP TABLE d2l_duplicate_student_contacts'; 
EXCEPTION 
WHEN OTHERS THEN 
  IF SQLCODE != -942 THEN 
    RAISE; 
  END IF; 
END;

 
DECLARE 
  -- We will store the school year calculated with a user defined function in this variable 
  calculated_school_year VARCHAR2(8); 
  -- Board BSID - Used to create provincially unique IDs, according to TELO Brightspace parent portal documentation
  board_bsid VARCHAR2(10); 
BEGIN 
  ---------------------------------------------------------------------------------- 
  -- TODO : Set your board BSID 
  ---------------------------------------------------------------------------------- 
  board_bsid := 'B1234'; 
  -- Call a UDF that determines current school year based on current date. 
  calculated_school_year := f_get_school_year_input_date(SYSDATE); 
  ---------------------------------------------------------------------------------- 
  -- Create temp table to store contact-student relationships. These relationships are 
  -- used to generate the contact XML file as well as the contact-student relationships file 
  ---------------------------------------------------------------------------------- 
  ---------------------------------------------------------------------------------- 
  -- Patrick TODO: need to adjust the field size based on the size of the fields on Trillium 
  -- student_surname => persons.preferred_surname 
  -- student_first_name => persons.preferred_first_name 
  -- student_oen_number => persons.oen_number 
  -- student_person_id=>student_contacts.student_person_id 
  -- contact_person_id=>student_contacts.contact_person_id 
  -- contact_surname => persons.preferred_surname 
  -- contact_first_name => persons.preferred_first_name 
  -- contact_email => person_telecom.email_account 
  CREATE TABLE d2l_student_contacts 
               ( 
                            student_surname    VARCHAR2(255) NOT NULL, 
                            student_first_name VARCHAR2(255) NOT NULL, 
                            student_oen_number VARCHAR2(255) NOT NULL, 
                            student_person_id  VARCHAR2(255) NOT NULL, 
                            contact_person_id  VARCHAR2(255) NOT NULL, 
                            contact_surname    VARCHAR2(255) NOT NULL, 
                            contact_first_name VARCHAR2(255) NOT NULL, 
                            contact_email      VARCHAR2(255) NOT NULL, 
                            --Order is important here, since contact_person_id is used in a group by later, we save an index by using it first, as the PK can be used
                            CONSTRAINT contact_student_pk PRIMARY KEY (contact_person_id,student_person_id)
               ); 
   
  -- Order is important here, since we select records with empty contact emails sometimes, this allows an index reuse if the email is placed first
  -- also, we will run a group by based on email,surname,first_name, in that order, so the key must be in that order to save indexes
  CREATE INDEX contact_email_lname_fname_idx 
  ON d2l_student_contacts 
               ( 
                            contact_email, 
                            contact_surname, 
                            contact_first_name 
               ); 
   
  --We filter by oen_number, so an index is used to speed up the process later on 
  CREATE INDEX student_oen_number_idx 
  ON d2l_student_contacts 
               ( 
                            student_oen_number 
               ); 
   
  ---------------------------------------------------------------------------------- 
  -- Create temp table to contacts with invalid data for easy examination 
  ---------------------------------------------------------------------------------- 
  ---------------------------------------------------------------------------------- 
  -- Patrick TODO: need to adjust the field size based on the size of the fields on Trillium 
  -- contact_surname => persons.preferred_surname 
  -- contact_first_name => persons.preferred_first_name 
  -- contact_email => person_telecom.email_account 
  CREATE TABLE d2l_invalid_student_contacts 
               ( 
                            contact_person_id  VARCHAR2(255) NOT NULL, 
                            contact_surname    VARCHAR2(255) NOT NULL, 
                            contact_first_name VARCHAR2(255) NOT NULL 
               ); 
   
  ---------------------------------------------------------------------------------- 
  -- Create temp table to contacts with duplicate data for easy examination 
  ---------------------------------------------------------------------------------- 
  ---------------------------------------------------------------------------------- 
  -- Patrick TODO: need to adjust the field size based on the size of the fields on Trillium 
  -- contact_surname => persons.preferred_surname 
  -- contact_first_name => persons.preferred_first_name 
  -- contact_email => person_telecom.email_account 
  CREATE TABLE d2l_duplicate_student_contacts 
               ( 
                            contact_surname    VARCHAR2(255) NOT NULL, 
                            contact_first_name VARCHAR2(255) NOT NULL, 
                            contact_email      VARCHAR2(255) NOT NULL 
               ); 
   
  ---------------------------------------------------------------------------------- 
  -- Insert-Select all tuples of student-contacts to build a view of the data which can be consulted later for debugging 
  -- We only select parents with valid contact information for students for whom parents have access to records
  ---------------------------------------------------------------------------------- 
  INSERT INTO d2l_student_contacts 
              ( 
                          student_surname, 
                          student_first_name, 
                          student_oen_number, 
                          student_person_id, 
                          contact_person_id, 
                          contact_surname, 
                          contact_first_name, 
                          contact_email 
              )
  -- We cleanup the case and any whitespaces to allow for easier duplicate matching later on
  -- Patrick TODO: What is the correct INITCAPS method for multibyte characters? ex: éric => Éric
  SELECT          ltrim(rtrim(INITCAP(sp.preferred_surname))), 
                  ltrim(rtrim(INITCAP(sp.preferred_first_name))), 
                  ltrim(rtrim(INITCAP(sp.oen_number))), 
                  sc.student_person_id, 
                  sc.contact_person_id, 
                  ltrim(rtrim(INITCAP(scp.preferred_surname))), 
                  ltrim(rtrim(INITCAP(scp.preferred_first_name))), 
                  ltrim(rtrim(lower(scpt.email_account))) 
  FROM            student_contacts sc 
  inner join      persons sp 
  ON              sc.student_person_id = sp.person_id 
  left outer join student_registrations sr 
  ON              sp.person_id = sr.person_id 
  AND             sc.guardian_flag = 'x' 
  AND             sc.access_to_records_flag = 'x' 
  AND             sc.primary_contact_priority <> '99' 
  AND             ( 
                                  sc.end_date IS NULL 
                  OR              sc.end_date > SYSDATE ) 
  inner join      persons scp 
  ON              sc.contact_person_id = scp.person_id 
  inner join      person_telecom scpt 
  ON              scp.person_id = scpt.person_id 
  AND             scpt.email_type_flag = 'x' 
  AND             ( 
                                  scpt.end_date IS NULL 
                  OR              scpt.end_date > SYSDATE ) 
                  --AND scpt.email_type='1' 
                  --AND scpt.casl='S' 
  AND             scpt.start_date = 
                  ( 
                         SELECT pa2.start_date 
                         FROM   person_telecom pa2 
                         WHERE  pa2.person_id = scpt.person_id 
                         AND    ROWNUM = 1 
                         AND    pa2.email_type_flag = 'x' 
                         AND    ( 
                                       pa2.end_date IS NULL 
                                OR     pa2.end_date > SYSDATE ) ) 
  WHERE           sr.school_year = calculated_school_year 
  AND             sr.status_indicator_code = 'Active' 
  AND             sr.school_type = 'Home' 
                  /* By law child does not require parent contact relationship. verified by Shawn Allenby TVO 23.06.2017
unless child gives explicit consent to adult via stud18_consent_flag_10 
*/ 
  AND             ( 
                                  sp.adult_flag <> 'x' 
                  OR              sc.stud18_consent_flag_10 = 1 ); 
   
  ---------------------------------------------------------------------------------- 
  -- Expose invalid parent records which have no email addresses, this should probably be a 
  ---------------------------------------------------------------------------------- 
  INSERT INTO d2l_invalid_student_contacts 
              ( 
                          contact_person_id, 
                          contact_surname, 
                          contact_first_name 
              ) 
  SELECT contact_person_id, 
         contact_surname, 
         contact_first_name 
  FROM   d2l_student_contacts 
  WHERE  contact_email = '' 
  AND    student_oen_number <> ''; 
   
  ---------------------------------------------------------------------------------- 
  -- Expose invalid parent records which are possible duplicates 
  ---------------------------------------------------------------------------------- 
  INSERT INTO d2l_duplicate_student_contacts 
              ( 
                          contact_email, 
                          contact_surname, 
                          contact_first_name 
              ) 
  SELECT   contact_email, 
           contact_surname, 
           contact_first_name 
  FROM     ( 
                    SELECT   contact_email, 
                             contact_surname, 
                             contact_first_name 
                    FROM     d2l_student_contacts 
                    WHERE    contact_email = '' 
                    AND      student_oen_number <> '' 
                    GROUP BY contact_person_id) 
  WHERE    contact_email = '' 
  AND      student_oen_number <> '' 
  GROUP BY no_identical.contact_email, 
           no_identical.contact_surname, 
           no_identical.contact_first_name 
  HAVING   count(*) >= 2; 
   
  ---------------------------------------------------------------------------------- 
  -- Select single occurrences of each parent record (we certainly have duplicates here, since we inserted
  -- a parent record per student 
  ---------------------------------------------------------------------------------- 
  -- Patrick TODO: need to find out how to export to XML
  SELECT DISTINCT board_bsid 
                                  || '_' 
                                  || contact_person_id AS "sourcedid/id", 
                  -- keep blank as per BrightSpace recommendation 17.11.2017 
                  ''                 AS "userid/@password", 
                  contact_email      AS userid, 
                  contact_surname    AS "name/n/family", 
                  contact_first_name AS "name/n/given", 
                  '_Parent'          AS "extension/userrole", 
                  contact_email      AS email 
  FROM            d2l_student_contacts 
  WHERE           contact_email <> '' 
  AND             student_oen_number <> ''; 
   
  ---------------------------------------------------------------------------------- 
  -- Select the parent-student relationships 
  ---------------------------------------------------------------------------------- 
  -- Patrick TODO: need to find out how to export to XML
  SELECT student_oen_number AS childid, 
         board_bsid 
                || '_' 
                || contact_person_id AS "sourcedid/id" 
  FROM   d2l_student_contacts 
  WHERE  contact_email <> '' 
  AND    student_oen_number <> ''; 

END;